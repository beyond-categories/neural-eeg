
# coding: utf-8

# ## Processing EEG data for the N170 Face Perception experiment using the MNE ("MEG and EEG") library in Python
#
# IPy Notebook by: Aalok S.
#
# Data: Beyond Categories (Bukach Lab), Univ. of Richmond
#
# Requires: `python>=3.5, mne, numpy, pandas, pylab, matplotlib`

# In[1]:


import os
import re
import pickle
from progressbar import progressbar
from glob import glob


# #### SciPy imports for scientific computing

# In[2]:


import numpy as np
import pylab as pl
import pandas as pd


# In[3]:


import mne


# In[4]:


datapath = os.path.join(os.getcwd(), "data")
logspath = os.path.join(os.getcwd(), "logs")


# In[5]:


subjects = {re.search("_s(.+?)-", x).group(1) for x in
            glob("logs/*")}
subjects -= {re.search("/(.+?).mne.epochs$", x).group(1) for x
             in glob("processed/*")}


# In[6]:

for subj in progressbar(subjects, redirect_stdout=True):

    print("Processing data for subject {sub}".format(sub=subj))

    try:
        raw = mne.io.read_raw_brainvision(glob("%s/*%s*.vhdr"%(datapath, subj))[0],
                                      verbose=0, preload=True).resample(256, npad='auto')
    except FileNotFoundError:
        print("Error reading EEG data for s%s"%subj)


    # In[7]:


    montage = raw.set_montage(mne.channels.read_montage("standard_1020"))


    # In[8]:


    try:
        log = pd.read_csv(glob("%s/*%s*.txt"%(logspath, subj))[0], delimiter='\t')
    except FileNotFoundError:
        print("Error reading logfile")


    # In[11]:


    raw_no_ref, _ = mne.set_eeg_reference(raw, [])


    # #### Filter data using pre-defined high-pass frequency values

    # In[12]:


    filtered = raw_no_ref.filter(.2, None, n_jobs=1, fir_design='firwin')#(.2, None)#19.8)
    print(filtered.info)


    # #### Pick good channels and run ICA

    # In[13]:


    picks_eeg = mne.pick_types(filtered.info, meg=False, eeg=True, eog=False,
                               stim=False, exclude='bads')


    # In[14]:


    ica = mne.preprocessing.ICA(n_components=25, method="fastica", random_state=7)
    ica = ica.fit(filtered, picks=picks_eeg, decim=3)
    filtered = ica.apply(filtered)


    # #### Low-pass filter

    # In[16]:


    filtered = raw_no_ref.filter(None, 19.8, n_jobs=1, fir_design='firwin')
    print(filtered.info)


    # #### Gather information about events and port codes from the text logfile

    # In[20]:


    data = (zip(log.iloc[11:]['Port Code'], log.iloc[11:]['Condition']))
    data = dict(data)


    # In[21]:


    event_id = {"response/correct": 201, "response/incorrect": 202}
    for event in list(range(1,81))+list(range(101,181)):
        event_id["stimulus/%s/%d"%(data[event].lower().replace(' ', ''), event)] = event


    # In[22]:


    events = mne.find_events(filtered)


    # #### Locate the epochs centered around events using the information collected in the previous step

    # In[23]:

    try:
        epochs = mne.Epochs(filtered, events, event_id=event_id, tmin=-.6, tmax=.9, preload=True)
        epochs, _ = epochs.equalize_event_counts(event_ids=[str(x) for x in list(range(1,81))+list(range(101,181))])

        # In[24]:

        with open("processed/%s.mne.epochs"%subj, 'wb') as out:
            pickle.dump(epochs, out)

    except ValueError as error:
        print("ERROR", error)
        continue
