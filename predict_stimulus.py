
# coding: utf-8

# ### Modeling EEG data from the N170 Face Perception experiment
# 
# IPy Notebook by: Aalok S.
# 
# Data: Beyond Categories (Bukach Lab), Univ. of Richmond
# 
# Requires: `python>=3.5, mne, numpy,` <s>pandas</s>, `pylab, matplotlib`

# In[1]:


import os
import pickle
import types
from progressbar import progressbar
from glob import glob


# #### SciPy imports for scientific computing

# In[2]:


import numpy as np
import pylab as pl
# import pandas as pd


# In[3]:


import mne


# In[4]:


import keras as ks
import keras.backend as K


# In[5]:


datapath = os.path.join(os.getcwd(), "data")
logspath = os.path.join(os.getcwd(), "logs")


# #### Prepare the data

# In[6]:


def unison_shuffle(a, b):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]


# In[7]:


subjects = ["2030",]
subj = subjects[0]


# In[8]:


for subj in subjects:
    with open("processed/{subj}.mne.epochs".format(subj=subj), 'rb') as infile:
        epochs = pickle.load(infile)


# In[9]:


events = epochs.events[:,2]
print("INFO: All events [shape]:", events.shape)
data = epochs.get_data()[:,:-1,:]
print("INFO: Raw data [shape]:", data.shape)
events,data = unison_shuffle(events,data)
print("LOG: Shuffled 'events' and 'data' in unison.")
stimuli_indices = np.array([row_index for row_index in range(len(events)) if events[row_index] not in {201,202}])
print("INFO: Indices at which stimuli are present [shape]:", stimuli_indices.shape)
events = events[stimuli_indices]
print("INFO: Events corresponding to stimuli [shape]:", events.shape)
data = epochs.get_data()[stimuli_indices]
print("INFO: Raw data corresponding to stimuli [shape]:", data.shape)


# In[10]:


event_types = {v: k.split('/')[1] for k, v in epochs.event_id.items() if v not in {201, 202}}
print("INFO: Stimulus <--> event type:", event_types)


# In[11]:


categories = list(set(event_types.values()))
categories = {categories[index]: 1+index for index in range(len(categories))}
# categories.update({None: 0})
# categories.update({'scrambledcars': 1})
print(categories)


# #### Train the model

# In[12]:


# @generator_wrapper
def data_generator(events=events, data=data, train_proportion=.8, mode='train'):
    if mode == 'train':
        lower = 0
        upper = int(train_proportion*len(events))
    elif mode == 'test':
        lower = int(train_proportion*len(events))
        upper = None
    else:
        raise ValueError("keyword argument 'mode' received illegal option: {recd}".format(recd=mode))
        
    data_sub, event_sub = unison_shuffle(data[lower:upper], events[lower:upper])
    xy = zip(data_sub, event_sub)
    
    while True:
        try:
            data_snap,event = next(xy)
        except StopIteration:
            xy = zip(data_sub, event_sub)
            data_snap,event = next(xy)
        x = np.array(data_snap).reshape(-1, *data_snap.shape)
        y = np.array([categories[event_types[event]]])
        yield [x,y]


# #### Build a model

# In[13]:


data.shape


# In[14]:


input_layer = ks.layers.Input(shape=data.shape[1:], name="input_1")


# In[15]:


layer = ks.layers.Flatten(name="flatten_1")(input_layer)


# In[16]:


layer = ks.layers.Dense(units=512, activation="relu", name="dense_512_1")(layer)


# In[17]:


layer = ks.layers.Dropout(rate=.25, name="dropout_1")(layer)


# In[18]:


layer = ks.layers.Dense(units=512, activation="relu", name="dense_512_2")(layer)


# In[19]:


layer = ks.layers.Dense(units=256, activation="relu", name="dense_256_1")(layer)


# In[20]:


output_layer = ks.layers.Dense(1+len(categories), activation="softmax", name="output_1")(layer)


# In[21]:


model = ks.models.Model(inputs=[input_layer], outputs=[output_layer], name="category_model")


# In[22]:


model.compile(loss=["sparse_categorical_crossentropy"], metrics=["sparse_categorical_accuracy"], optimizer="adam")


# In[23]:


model.summary()
print("INFO: metrics_names: ", model.metrics_names)


# #### Train the model

# In[24]:


history = model.fit_generator(generator=data_generator(), epochs=32, steps_per_epoch=128, verbose=True)


# In[25]:


metrics = model.evaluate_generator(data_generator(mode='train'), steps=1000)
print("INFO: loss={loss}, acc={acc}".format(loss=metrics[0], acc=metrics[1]))
metrics = model.evaluate_generator(data_generator(mode='test'), steps=1000)
print("INFO: loss={loss}, acc={acc}".format(loss=metrics[0], acc=metrics[1]))


# In[26]:


sample_indices = np.random.randint(0, high=100, size=(20,))
print(sample_indices)
labels = [categories[event_types[item]] for item in events[sample_indices]]
predictions = [np.argmax(arr) for arr in model.predict(x=data[sample_indices])]
print("EVAL: labels:", labels)
print("EVAL: preds: ", predictions)
print("EVAL: dict:  ", categories)

